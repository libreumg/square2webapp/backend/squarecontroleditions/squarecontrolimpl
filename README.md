squareControl is a R library to manage database access, execution of R scripts and writing results back to the database. 

squareControl works in combination with Square², a web application for data analysis tasks.
