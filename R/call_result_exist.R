#' check, if any results exist for a report
#'
#' @param reportconfig the repository configuration implementing the
#'                     `.SquareResultIO`
#'
#' @return an "entry file" with the reports
#' @export
#'
#' @importFrom squarerepositoryspec getAvailReportFiles doResultsExist
#'
#' @author Jörg Henke
#'
call_result_exist <- function(reportconfig) {
  required_repository_keys <- c(
    ".SquareResultIO"
  )

  impllist <- parse_reportconfig(reportconfig, required_repository_keys)

  doResultsExist(impllist$SquareResultIO)
}
