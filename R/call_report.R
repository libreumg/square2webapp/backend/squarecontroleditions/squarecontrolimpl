#' get the report file referenced by the rel_path
#'
#' @param reportconfig the repository configuration implementing the
#'                     `.SquareResultIO`
#' @param rel_path the file reference
#' @param admin defaults to FALSE; if TRUE, returns admin only files also
#'
#' @return the file
#' @export
#'
#' @importFrom squarerepositoryspec getReportFile
#'
#' @author Jörg Henke
#'
call_report <- function(reportconfig, rel_path, admin = FALSE) {
  required_repository_keys <- c(
    ".SquareResultIO"
  )

  impllist <- parse_reportconfig(reportconfig, required_repository_keys)

  getReportFile(impllist$SquareResultIO, rel_path, admin)
}
