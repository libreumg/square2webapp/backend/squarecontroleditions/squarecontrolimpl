# SquareControlImpl 0.6.1
- signature corrections

# SquareControlImpl 0.6.0
- omit breakdown of calculation
- support call_user_reports

# SquareControlImpl 0.5.13
- close the `SquareStudydataLoader` on leaving / aborting computations to avoid
  study data being stored on the compute node.
  
# SquareControlImpl 0.5.12
- added call for progress

# SquareControlImpl 0.5.11
- added flag to decide about monolithic report files

# SquareControlImpl 0.5.10
- fixed report calculation failure on missing report folder

# SquareControlImpl 0.5.9
- fixed calling an undefined object

# SquareControlImpl 0.5.7
- fixed version number of `squarerepositoryspec`

# SquareControlImpl 0.5.7
- too old version of `squarerepositoryspec`

# SquareControlImpl 0.5.6

- Fixed typos and `NAMESPACE` issues.
